import sys
import os
from PIL import Image, ImageDraw

def draw_corners(img, x, y, size):
    x1, x2, y1, y2 = 0, size, 0, size
    if x != 0:
        x1 = img.size[0] - size
        x2 = img.size[0] -1
    if y != 0:
        y1 = img.size[1] - size
        y2 = img.size[1] -1

    draw = ImageDraw.Draw(img)
    fill_color = "white"
    if x == 0 and y == 0:
        draw.line((x1, y1, x1, y2), fill=fill_color)
        draw.line((x1, y1, x2, y1), fill=fill_color)
        draw.line((x1+1, y1, x1+1, y2), fill=fill_color)
        draw.line((x1, y1+1, x2, y1+1), fill=fill_color)
    elif y == 0:
        draw.line((x2, y1, x2, y2), fill=fill_color)
        draw.line((x1, y1, x2, y1), fill=fill_color)
        draw.line((x2-1, y1, x2-1, y2), fill=fill_color)
        draw.line((x1, y1+1, x2, y1+1), fill=fill_color)
    elif x == 0:
        draw.line((x1, y1, x1, y2), fill=fill_color)
        draw.line((x1, y2, x2, y2), fill=fill_color)
        draw.line((x1+1, y1, x1+1, y2), fill=fill_color)
        draw.line((x1, y2-1, x2, y2-1), fill=fill_color)
    else:
        draw.line((x2, y1, x2, y2), fill=fill_color)
        draw.line((x1, y2, x2, y2), fill=fill_color)
        draw.line((x2-1, y1, x2-1, y2), fill=fill_color)
        draw.line((x1, y2-1, x2, y2-1), fill=fill_color)

if __name__ == '__main__':
    filelist = os.listdir(".")
    for filename in filelist:
        if filename[-3:] != 'png':
            continue
    
        img = Image.open(filename)
        draw = ImageDraw.Draw(img)
        size = int(round(2 * 0.0393700787 * 300))
    
        draw_corners(img, 0, 0, size)
        draw_corners(img, img.width-1, 0, size)
        draw_corners(img, 0, img.height-1, size)
        draw_corners(img, img.width-1, img.height-1, size)
        img.save(filename, dpi=(300, 300))