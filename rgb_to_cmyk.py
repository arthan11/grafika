from PIL import Image


for i in range(1, 25):
    x = '{:02d}'.format(i)
    print x
    path_to_image = 'Claustrophobia{}.png'.format(x)
    path_to_image_2 = 'Claustrophobia{}.tif'.format(x)
    
    image = Image.open(path_to_image)
    
    print image.mode
    if image.mode in ['RGB', 'RGBA']:
        cmyk_image = image.convert('CMYK')
        cmyk_image.save(path_to_image_2, dpi=(300,300))