import sys
import glob
from os.path import join
from PIL import Image
from math import ceil, floor


class ImgJoiner(object):
    def join(self, dir, file_mask, columns=1):
        files = glob.glob(join(dir, file_mask), recursive=True)
        files_count = len(files)
        if files_count == 0:
            print('No files found!')
            return

        files.sort()
        src_img = Image.open(files[0])
        [tile_width, tile_height] = src_img.size
        print(tile_width, tile_height)

        rows = ceil(files_count / columns)
        print(columns, rows)
        img = Image.new(mode="RGB", size=(tile_width * columns, tile_height * rows), color=(255, 255, 255))
        for idx, filename in enumerate(files):
            x = idx % columns * tile_width
            y = floor(idx / columns) * tile_height
            if idx > 0:
                src_img = Image.open(filename)
            img.paste(src_img, (x, y))
        img.save(dir + '.jpg')

if __name__ == '__main__':
    img = ImgJoiner()
    # img.join('else', '*.png', 4)
    # img.join('dino', '*.png', 9)
    img.join('events', '*.png', 11)
