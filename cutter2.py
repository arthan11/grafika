import sys
from PIL import Image, ImageEnhance, ImageDraw
import pytesseract

class ImgCutter(object):
    def __init__(self, src_file_name):
        self.src_file_name = src_file_name
        self.dst_mask = '{}x{}.png'
        self.columns = 1
        self.rows = 1

    def set_contrast(self, img, factor=1.5):
        enhancer = ImageEnhance.Contrast(img)
        return enhancer.enhance(factor)

    def draw_corners(self, img, x, y, size):
        x1, x2, y1, y2 = 0, size, 0, size
        if x != 0:
            x1 = img.size[0] - size
            x2 = img.size[0] -1
        if y != 0:
            y1 = img.size[1] - size
            y2 = img.size[1] -1

        draw = ImageDraw.Draw(img)
        if x == 0 and y == 0:
            draw.line((x1, y1, x1, y2), fill="black")
            draw.line((x1, y1, x2, y1), fill="black")
            draw.line((x1+1, y1, x1+1, y2), fill="black")
            draw.line((x1, y1+1, x2, y1+1), fill="black")
        elif y == 0:
            draw.line((x2, y1, x2, y2), fill="black")
            draw.line((x1, y1, x2, y1), fill="black")
            draw.line((x2-1, y1, x2-1, y2), fill="black")
            draw.line((x1, y1+1, x2, y1+1), fill="black")
        elif x == 0:
            draw.line((x1, y1, x1, y2), fill="black")
            draw.line((x1, y2, x2, y2), fill="black")
            draw.line((x1+1, y1, x1+1, y2), fill="black")
            draw.line((x1, y2-1, x2, y2-1), fill="black")
        else:
            draw.line((x2, y1, x2, y2), fill="black")
            draw.line((x1, y2, x2, y2), fill="black")
            draw.line((x2-1, y1, x2-1, y2), fill="black")
            draw.line((x1, y2-1, x2, y2-1), fill="black")

    def normalize(self, src_img):
        new_width, new_height = 981, 1512
        new_img = Image.new("RGB", (new_width, new_height), "white")
        w, h = src_img.size
        w = w * 2
        h = h * 2
        src_img = src_img.resize((w, h), Image.ANTIALIAS)
        new_img.paste(src_img, (0, 0))

        # biale trojkaty w rogach
        draw = ImageDraw.Draw(new_img)
        fill_clr = (255, 255, 255)
        size = 50
        draw.polygon([(0, 0), (0, size), (size, 0)], fill = fill_clr)
        draw.polygon([(new_width, 0), (new_width - size, 0), (new_width, size)], fill = fill_clr)
        draw.polygon([(0, new_height), (size, new_height), (0, new_height - size)], fill = fill_clr)
        draw.polygon([(new_width, new_height), (new_width, new_height - size), (new_width - size, new_height)], fill = fill_clr)
        del draw

        self.draw_corners(new_img, 0, 0, size)
        self.draw_corners(new_img, new_width, 0, size)
        self.draw_corners(new_img, 0, new_height, size)
        self.draw_corners(new_img, new_width, new_height, size)
        return new_img
        #new_img.show()
        #sys.exit()

    def get_name(self, img):
        new_img = img.crop((80, 60, 410, 120))
        new_img = new_img.resize((new_img.size[0]*3, new_img.size[1]*3), Image.ANTIALIAS)
        #new_img = self.set_contrast(new_img, 2.0)
        name = pytesseract.image_to_string(new_img, lang='pol')
        name = ''#name.replace('\n',' ').strip().replace(':', '').replace('?', '').replace('*', '')
        #print name
        #new_img.show()
        #sys.exit()
        return name
        
    def cut(self):
        src_img = Image.open(self.src_file_name)
        [x,y] = src_img.size
        x = x/self.columns
        y = y/self.rows

        for j in range(self.rows):
            for i in range(self.columns):
                x1 = i * x
                y1 = j * y
                x2 = (i+1) * x
                y2 = (j+1) * y
                box = (x1, y1, x2, y2)
                dst_img = src_img.crop(box)
                #dst_img = self.set_contrast(dst_img, 1.5)
                #dst_file_name = self.dst_mask.format(j, i)
                #print dst_file_name
                #print self.get_name(dst_img)
                dst_file_name = self.get_name(dst_img)
                print dst_file_name
                if dst_file_name == '':
                    dst_file_name = self.dst_mask.format(j, i)
                else:
                    dst_file_name += '.png'

                #sys.exit()
                #dst_img.save(dst_file_name, dpi=(300.0, 300.0))
                dst_img = self.normalize(dst_img)
                dst_img.save(dst_file_name, dpi=(600.0, 600.0))


if __name__ == '__main__':
    name = 'cut'
    src_file_name = name + '.jpg'
    img = ImgCutter(src_file_name)
    img.columns = 6
    img.rows = 3
    img.dst_mask = name + '_{}_{}.png'
    img.cut()
    