#convert -density 100 xxx.pdf -flatten xxx.jpg

from wand.image import Image, ALPHA_CHANNEL_TYPES
from wand.api import library
from wand.color import Color
import os, os.path, sys
from download import downloader

class Converter():
    def __init__(self):
        self.prev = None
        self.pages_count = 0

    def GetPagesCount(self, source_file):
        ret = 0
        #try:
        if True:
            with Image(filename=source_file) as img:
                ret = len(img.sequence)
        #except Exception as e:
        #    pass
        return ret

    def pdf2jpg(self, source_file, target_file, page_nr):
        RESOLUTION = 200
        ret = True
        
        if True: #try:
            with Image(filename=source_file, resolution=(RESOLUTION,RESOLUTION)) as img:
                library.MagickSetImageAlphaChannel(img.wand, ALPHA_CHANNEL_TYPES.index('flatten'))
                dst_file = target_file[:-4] + '-large' + target_file[-4:]
                img.save(filename = dst_file)

                img_width = img.width
                img_height = img.height
                dest_width = 460
                ratio = float(dest_width) / img_width
                img.resize(int(dest_width), int(ratio * img_height))
                img.save(filename = target_file)
                
                dest_height = 73
                ratio = float(dest_height) / img_height
                img.resize(int(ratio * img_width), int(dest_height))
                dst_file = target_file[:-4] + '-prev' + target_file[-4:]
                if not self.prev:
                    w = img.width * 2
                    rows = int(round(self.pages_count /2.0)+1)
                    h = dest_height * rows
                    self.prev = Image(width=w, height=h, background=Color('white'))
                    self.prev.type = 'truecolormatte'
                if (page_nr > 1):
                    page_nr = page_nr + 1
                t = ((page_nr-1) / 2) * dest_height
                l = img.width if (page_nr % 2 == 0) else 0
                img.type = 'truecolormatte'
                self.prev.composite(img, left=l, top=t)

        #except Exception as e:
        #    ret = False

        return ret

    def convert(self, source_file, target_dir, ext):
        self.pages_count = self.GetPagesCount(source_file)
        print source_file
        print 'stron:', self.pages_count
        #self.pages_count = 2
        
        if self.pages_count > 0:
            if not os.path.exists(target_dir):
                os.makedirs(target_dir)

        for i in range(self.pages_count):
            print i+1
            src_file = '%s[%d]' % (source_file, i)
            dst_file = target_dir + str(i+1) + ext
            self.pdf2jpg(src_file, dst_file, i+1)
        if self.prev:
            self.prev.save(filename = target_dir + 'preview' + ext)

if __name__ == "__main__":
    BASE_URL = 'http://www.luzino.pl/02biuletyny/%sbiuletyn/%dbiul.pdf'
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))

    #nr = 123
    for nr in range(134, 135):
        print 'nr:', nr
        #continue
        src = os.path.join(BASE_DIR, 'luzino', 'static', 'dokumenty' , 'biul%d.pdf' % nr)
        dst = os.path.join(BASE_DIR, 'luzino', 'static', 'pages', 'biul%d' % nr, '')
        
        url = BASE_URL % ('15', nr)
        #d = downloader()
        #d.get(url, src)
        
        c = Converter()
        c.convert(src, dst, '.jpg')
