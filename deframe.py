from PIL import Image
import os

def get_avg(img_rbg, x, y):
    r, g, b = img_rbg.getpixel((x, y))
    return (r + g + b) / 3

def is_rgb_avg_white(img_rbg, x, y):
    return get_avg(img_rbg, x, y) > 150

def is_rgb_avg_black(img_rbg, x, y):
    return get_avg(img_rbg, x, y) < 100

def get_pic_frame(img):
    img_rgb = img.convert('RGB')
           
    frame = [0]*4
    x, y = 0, img.height / 2
    while is_rgb_avg_black(img_rgb, x, y):
        x += 1
    frame[0] = x
    
    x, y = img.width - 1, img.height / 2
    while is_rgb_avg_black(img_rgb, x, y):
        x -= 1
    frame[2] = x
    
    x, y = img.width / 2, 0
    while is_rgb_avg_black(img_rgb, x, y):
        y += 1
    frame[1] = y
    
    x, y = img.width / 2, img.height - 1
    while is_rgb_avg_black(img_rgb, x, y):
        y -= 1
    frame[3] = y

    return frame 


filename = 'ALL Tajemnice BACK.png'
folder = 'ready'

img = Image.open(filename)
pic_frame = get_pic_frame(img)
print (pic_frame)                  

filelist = os.listdir(folder)
for filename in filelist:
    filepath = '{}\\{}'.format(folder, filename)
    print filepath
    img_pic = Image.open(filepath)
    img_pic = img_pic.crop(pic_frame)
    if filepath[-3:] == 'jpg':
        filepath = filepath[:-3] + 'png'
    img_pic.save(filepath, dpi=(300, 300))
