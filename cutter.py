import sys
from PIL import Image
from PIL import ImageEnhance

class ImgCutter(object):
    def __init__(self, src_file_name):
        self.src_file_name = src_file_name
        self.dst_mask = '{}x{}.png'
        self.columns = 1
        self.rows = 1

    def set_contrast(self, img, factor):
        enhancer = ImageEnhance.Contrast(img)
        return enhancer.enhance(1.5)
        
    def cut(self):
        src_img = Image.open(self.src_file_name)
        [x,y] = src_img.size
        x = x/self.columns
        y = y/self.rows

        for j in range(self.rows):
            for i in range(self.columns):
                x1 = i * x
                y1 = j * y
                x2 = (i+1) * x
                y2 = (j+1) * y
                box = (x1, y1, x2, y2)
                dst_img = src_img.crop(box)
                #dst_img = self.set_contrast(dst_img, 1.5)
                dst_file_name = self.dst_mask.format(j, i)
                print dst_file_name
                dst_img.save(dst_file_name, dpi=(300.0, 300.0))


if __name__ == '__main__':
    name = 'card'
    src_file_name = name + '.jpg'
    img = ImgCutter(src_file_name)
    img.columns = 7
    img.rows = 3
    img.dst_mask = name + '_{}_{}.png'
    img.cut()
    